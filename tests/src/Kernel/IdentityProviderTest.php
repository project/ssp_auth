<?php

namespace Drupal\Tests\ssp_auth\Kernel;

use Drupal\ssp_auth\Entity\IdentityProvider;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the identity provider config entity.
 *
 * @group ssp_auth
 */
class IdentityProviderTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'ssp_auth',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installEntitySchema('identity_provider');
  }

  /**
   * Tests the identity provider export properties.
   */
  public function testIdentityProviderPropertiesToExport() {
    $identityProvider = IdentityProvider::create([]);
    $expectedPropertiesToExport = [
      'id',
      'label',
      'idpKey',
      'userNameAttribute',
      'userEmailAttribute',
      'defaultRoles',
      'autoProvision',
    ];

    $availablePropertiesToExport = $identityProvider->getEntityType()->getPropertiesToExport();
    foreach ($expectedPropertiesToExport as $property) {
      $this->assertContains($property, $availablePropertiesToExport);
    }
  }

}
