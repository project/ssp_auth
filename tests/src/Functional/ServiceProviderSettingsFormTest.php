<?php

namespace Drupal\Tests\ssp_auth\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the service provider settings form.
 *
 * @group ssp_auth
 */
class ServiceProviderSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public static $modules = ['ssp_auth'];

  /**
   * Tests the service provider settings form.
   */
  public function testsServiceProviderSettingsForm() {
    $settingsFormUrl = Url::fromRoute('ssp_auth.service_provider_settings_form');
    $this->drupalGet($settingsFormUrl);
    $this->assertText('Access denied');

    $this->drupalLogin($this->createUser(['administer service provider settings']));
    $this->drupalGet($settingsFormUrl);
    $this->assertSession()->checkboxChecked('general_enable');
    $this->assertSession()->checkboxNotChecked('general_match_existing');
    $this->assertSession()->fieldExists('service_provider_id');
    $this->assertSession()->checkboxNotChecked('debugging_enable');

    $this->getSession()->getPage()->uncheckField('general_enable');
    $this->getSession()->getPage()->fillField('service_provider_id', 'test service id');
    $this->getSession()->getPage()->checkField('general_match_existing');
    $this->getSession()->getPage()->checkField('debugging_enable');
    $this->getSession()->getPage()->pressButton('Submit');

    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->assertSession()->checkboxNotChecked('general_enable');
    $this->assertSession()->checkboxChecked('general_match_existing');
    $this->assertSession()->fieldValueEquals('service_provider_id', 'test service id');
    $this->assertSession()->checkboxChecked('debugging_enable');
  }
}
