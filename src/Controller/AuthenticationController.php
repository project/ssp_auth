<?php

namespace Drupal\ssp_auth\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Url;
use Drupal\externalauth\ExternalAuthInterface;
use Drupal\ssp_auth\Entity\IdentityProviderInterface;
use Psr\Log\LoggerInterface;
use SimpleSAML\Auth\Simple;
use SimpleSAML\Auth\Source;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\ssp_auth\Event\LoginEvent;

/**
 * Provides the pages to handle user authentication.
 */
class AuthenticationController extends ControllerBase {

  protected $instance;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The ExternalAuth service.
   *
   * @var \Drupal\externalauth\ExternalAuth
   */
  protected $externalauth;

  /**
   * The URL generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * AuthenticationController constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\externalauth\ExternalAuthInterface $externalauth
   *   The ExternalAuth service.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The url generator.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(RequestStack $request_stack, ExternalAuthInterface $externalauth, UrlGeneratorInterface $url_generator, EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, LoggerInterface $logger, EventDispatcherInterface $event_dispatcher) {
    $this->requestStack = $request_stack;
    $this->externalauth = $externalauth;
    $this->urlGenerator = $url_generator;
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->logger = $logger;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('externalauth.externalauth'),
      $container->get('url_generator'),
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('logger.factory')->get('ssp_auth'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Initiates user login.
   *
   * @param \Drupal\ssp_auth\Entity\IdentityProviderInterface $identity_provider
   *   The identity provider.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\externalauth\Exception\ExternalAuthRegisterException
   */
  public function login(IdentityProviderInterface $identity_provider) {
    if (!$this->config($this->config('ssp_auth.serviceprovidersettings')->get('general_enable'))) {
      $this->logger->warning('Trying to authenticate with %identity_provider when SSO is disabled.', ['%identity_provider' => $identity_provider->getLabel()]);

      return $this->redirect('user.login');;
    }

    $settings = [
      'ReturnTo' => Url::fromRoute('ssp_auth.login', ['identity_provider' => $identity_provider->getId()], ['absolute' => TRUE])
        ->toString(),
      'saml:idp' => $identity_provider->getIdpKey(),
    ];

    $this->getSimpleSamlInstance()->requireAuth($settings);

    return $this->authenticate($identity_provider);
  }

  /**
   * Redirects the user after login.
   *
   * @param \Drupal\ssp_auth\Entity\IdentityProviderInterface $identity_provider
   *   The identity provider.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\externalauth\Exception\ExternalAuthRegisterException
   */
  public function authenticate(IdentityProviderInterface $identity_provider) {
    if (!$this->instance->isAuthenticated()) {
      $this->messenger->addError('You are not authenticated with the identity provider.');

      return $this->redirect('user.login');
    }

    $attributes = $this->instance->getAttributes();

    // Check the user name attribute.
    if (!$userName = $this->getUserNameAttribute($identity_provider, $attributes)) {
      $this->logger->error('User name attribute in %identity_provider is not available in the attributes.', ['%identity_provider' => $identity_provider->getLabel()]);
      $this->messenger->addError($this->t('An error occurred during the login process, please contact administrator.'));

      return $this->redirect('user.login');
    }

    // Check the user mail attribute.
    if (!$userMail = $this->getUserMailAttribute($identity_provider, $attributes)) {
      $this->logger->error('User mail attribute in %identity_provider is not available in the attributes.', ['%identity_provider' => $identity_provider->getLabel()]);
      $this->messenger->addError($this->t('An error occurred during the login process, please contact administrator.'));

      return $this->redirect('user.login');
    }

    $defaultRoles = $identity_provider->getDefaultRoles();
    $provision = $identity_provider->getAutoProvision();

    if ($this->externalLoginRegister($userName, $userMail, $defaultRoles, $provision)) {

      return $this->redirect('user.page');
    }

    $this->messenger->addError($this->t('An error occurred during the login process, please contact administrator.'));

    return $this->redirect('user.login');
  }

  /**
   * Returns the user name from the identity provider or FALSE.
   *
   * @param \Drupal\ssp_auth\Entity\IdentityProviderInterface $identityProvider
   *   The identity provider.
   * @param array $attributes
   *   Array of attributes from the identity provider.
   *
   * @return bool|string
   *   User name or FALSE.
   */
  protected function getUserNameAttribute(IdentityProviderInterface $identityProvider, array $attributes) {
    if (isset($attributes[$identityProvider->getUserNameAttribute()])) {
      return current($attributes[$identityProvider->getUserNameAttribute()]);
    }

    return FALSE;
  }

  /**
   * Returns the user mail from the identity provider or FALSE.
   *
   * @param \Drupal\ssp_auth\Entity\IdentityProviderInterface $identityProvider
   *   The identity provider.
   * @param array $attributes
   *   Array of attributes from the identity provider.
   *
   * @return bool|string
   *   User mail or FALSE.
   */
  protected function getUserMailAttribute(IdentityProviderInterface $identityProvider, array $attributes) {
    if (isset($attributes[$identityProvider->getUserEmailAttribute()])) {
      return current($attributes[$identityProvider->getUserEmailAttribute()]);
    }

    return FALSE;
  }

  /**
   * Returns a SimpleSAML Simple class instance.
   *
   * @return \SimpleSAML\Auth\Simple|NULL
   *   The SimpleSAML Simple instance.
   */
  protected function getSimpleSamlInstance() {
    if (!empty($this->instance)) {
      return $this->instance;
    }
    $config = $this->config('ssp_auth.serviceprovidersettings');
    $auth_source = $config->get('service_provider_id');

    $this->instance = new Simple($auth_source);

    return $this->instance;
  }

  /**
   * Retrieve list of authentication sources.
   *
   * @return array
   *   The id of all authentication sources.
   */
  protected function getSource() {
    return Source::getSources();
  }

  /**
   * Log in and optionally register a user based on the username provided.
   *
   * @param $userName
   * @param $email
   * @param $roles
   * @param bool $provision
   *
   * @return bool|\Drupal\Core\Entity\EntityInterface|\Drupal\user\UserInterface|mixed
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\externalauth\Exception\ExternalAuthRegisterException
   */
  protected function externalLoginRegister($userName, $email, $roles, $provision) {
    $account = $this->externalauth->login($userName, 'ssp_auth');

    $user = $this->entityTypeManager
      ->getStorage('user')
      // External auth hardcodes a prefix.
      // @see https://www.drupal.org/project/externalauth/issues/2798323
      ->loadByProperties(['name' => 'ssp_auth_' . $userName]);
    $user = $user ? reset($user) : FALSE;

    if (!$account && $user && $this->config('ssp_auth.serviceprovidersettings')->get('general_match_existing')) {
      $account = $this->externalauth->linkExistingAccount($userName, 'ssp_auth', $user);
    }

    if (!$account && $provision) {
      $account = $this->externalauth->register($userName, 'ssp_auth', ['mail' => $email]);

      foreach ($roles as $role) {
        $account->addRole($role);
      }

      $account->save();
      $this->externalauth->userLoginFinalize($account, $userName, 'ssp_auth');
    }

    $event = new LoginEvent($account);
    $this->eventDispatcher->dispatch(LoginEvent::LOGIN_EVENT, $event);

    return $account;
  }

}
