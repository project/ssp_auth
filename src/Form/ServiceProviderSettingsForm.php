<?php

namespace Drupal\ssp_auth\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ServiceProviderSettingsForm.
 */
class ServiceProviderSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'service_provider_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ssp_auth.serviceprovidersettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ssp_auth.serviceprovidersettings');

    $form['general'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('General settings'),
      '#collapsible' => FALSE,
    ];

    $form['general']['general_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable authentication via SimpleSAMLphp'),
      '#description' => $this->t('Enable authentication via SimpleSAMLphp.'),
      '#default_value' => $config->get('general_enable'),
    ];

    $form['general']['general_match_existing'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Match existing account with identity provider user'),
      '#description' => $this->t('Matches the existing user name with the user name from the identity provider attributes.'),
      '#default_value' => $config->get('general_match_existing'),
    ];

    $form['service_provider'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Service Provider'),
      '#collapsible' => FALSE,
    ];

    $form['service_provider']['service_provider_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service Provider ID'),
      '#maxlength' => 255,
      '#default_value' => $config->get('service_provider_id'),
      '#description' => $this->t("The id of the service provider configured in the authsources.php."),
      '#required' => TRUE,
    ];

    $form['debugging'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Debugging'),
      '#collapsible' => FALSE,
    ];

    $form['debugging']['debugging_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debugging'),
      '#description' => $this->t('Log debugging information'),
      '#default_value' => $config->get('debugging_enable'),
      '#weight' => '0',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('ssp_auth.serviceprovidersettings')
      ->set('general_enable', $form_state->getValue('general_enable'))
      ->set('debugging_enable', $form_state->getValue('debugging_enable'))
      ->set('service_provider_id', $form_state->getValue('service_provider_id'))
      ->set('general_match_existing', $form_state->getValue('general_match_existing'))
      ->save();
  }

}
