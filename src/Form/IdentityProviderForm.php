<?php

namespace Drupal\ssp_auth\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\RoleInterface;

/**
 * Class IdentityProviderForm.
 */
class IdentityProviderForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $identityProvider = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $identityProvider->label(),
      '#description' => $this->t("Label for the Identity provider."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $identityProvider->id(),
      '#machine_name' => [
        'exists' => '\Drupal\ssp_auth\Entity\IdentityProvider::load',
      ],
      '#disabled' => !$identityProvider->isNew(),
    ];

    $form['idpKey'] = [
      '#type' => 'textfield',
      '#title' => 'Identity provider key',
      '#required' => TRUE,
      '#description' => $this->t('The identity provider key.'),
      '#default_value' => $identityProvider->getIdpKey(),
    ];

    $form['userNameAttribute'] = [
      '#type' => 'textfield',
      '#title' => 'User name attribute',
      '#required' => TRUE,
      '#description' => $this->t('The user name attribute identifier.'),
      '#default_value' => $identityProvider->getUserNameAttribute(),
    ];

    $form['userEmailAttribute'] = [
      '#type' => 'textfield',
      '#title' => 'User email attribute',
      '#required' => TRUE,
      '#description' => $this->t('The user email attribute identifier.'),
      '#default_value' => $identityProvider->getUserEmailAttribute(),
    ];

    $roles = array_map('\Drupal\Component\Utility\Html::escape', user_role_names(TRUE));
    // Remove authenticated role. This role can not be assigned manually.
    unset($roles[RoleInterface::AUTHENTICATED_ID]);

    $form['defaultRoles'] = [
      '#type' => 'checkboxes',
      '#options' => $roles,
      '#multiple' => TRUE,
      '#title' => $this->t('Default roles'),
      '#description' => $this->t('Roles that should be assigned by default.'),
      '#default_value' => $identityProvider->getDefaultRoles(),

    ];

    $form['autoProvision'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto provision'),
      '#description' => $this->t('Create users without account automatically.'),
      '#default_value' => $identityProvider->getAutoProvision(),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $identity_provider = $this->entity;
    $status = $identity_provider->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Identity provider.', [
          '%label' => $identity_provider->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Identity provider.', [
          '%label' => $identity_provider->label(),
        ]));
    }
    $form_state->setRedirectUrl($identity_provider->toUrl('collection'));
  }

}
