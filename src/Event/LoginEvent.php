<?php

namespace Drupal\ssp_auth\Event;

use Drupal\user\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event that is fired after the user has been logged in.
 */
class LoginEvent extends Event {

  /**
   * Fires when the user has been logged in.
   *
   * @Event
   */
  const LOGIN_EVENT = 'ssp_auth_user_login';

  /**
   * The User.
   *
   * @var \Drupal\user\Entity\User
   */
  public $user;

  /**
   * Constructs the object.
   *
   * @param \Drupal\user\Entity\User $user
   *   The User object.
   */
  public function __construct(User $user) {
    $this->user = $user;
  }

  /**
   * Get the user.
   *
   * @return \Drupal\user\Entity\User
   *   Returns the user.
   */
  public function getUser() {
    return $this->user;
  }

}
