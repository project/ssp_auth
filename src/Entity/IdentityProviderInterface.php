<?php

namespace Drupal\ssp_auth\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Identity provider entities.
 */
interface IdentityProviderInterface extends ConfigEntityInterface {

  /**
   * Returns the unique identifier.
   *
   * @return int
   *   The identifier.
   */
  public function getId();

  /**
   * Sets the string unique identifier.
   *
   * @param int $id
   *   The string identifier.
   *
   * @return $this
   */
  public function setId($id);

  /**
   * Returns the administrative label for the entity type.
   *
   * @return string
   *   The label.
   */
  public function getLabel();

  /**
   * Sets the label.
   *
   * @param string $label
   *   The label of the layout.
   *
   * @return $this
   */
  public function setLabel($label);

  /**
   * Returns the identity provider key from the metadata entity id.
   *
   * @return string
   *   The identity provider entity id key.
   */
  public function getIdpKey();

  /**
   * Sets the identity provider key from the metadata entity id.
   *
   * @param string $idpKey
   *   The identity provider entity id key.
   *
   * @return $this
   */
  public function setIdpKey($idpKey);

  /**
   * Returns the user name attribute.
   *
   * @return string
   *   The user name attribute.
   */
  public function getUserNameAttribute();

  /**
   * Sets the user name attribute.
   *
   * @param string $userNameAttribute
   *   The user name attribute.
   *
   * @return $this
   */
  public function setUserNameAttribute($userNameAttribute);

  /**
   * Returns the user mail attribute.
   *
   * @return string
   *   The user mail attribute.
   */
  public function getUserEmailAttribute();

  /**
   * Sets the user mail attribute.
   *
   * @param string $userEmailAttribute
   *   The user mail attribute.
   *
   * @return $this
   */
  public function setUserEmailAttribute($userEmailAttribute);

  /**
   * Returns the default user roles.
   *
   * @return array
   *   Array of user roles.
   */
  public function getDefaultRoles();

  /**
   * Sets the default user roles.
   *
   * @param array $defaultRoles
   *   The default user roles.
   *
   * @return $this
   */
  public function setDefaultRoles(array $defaultRoles);

  /**
   * Returns if the user should be auto provisioned.
   *
   * @return bool
   *   TRUE if the user should be auto provisioned.
   */
  public function getAutoProvision();

  /**
   * Sets if the user should be auto provisioned.
   *
   * @param bool $autoProvision
   *   Whether the user should be auto provisioned.
   *
   * @return $this
   */
  public function setAutoProvision($autoProvision);

}
