<?php

namespace Drupal\ssp_auth\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Identity provider entity.
 *
 * @ConfigEntityType(
 *   id = "identity_provider",
 *   label = @Translation("Identity provider"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ssp_auth\IdentityProviderListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ssp_auth\Form\IdentityProviderForm",
 *       "edit" = "Drupal\ssp_auth\Form\IdentityProviderForm",
 *       "delete" = "Drupal\ssp_auth\Form\IdentityProviderDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\ssp_auth\IdentityProviderHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "identity_provider",
 *   admin_permission = "administer identity providers",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "idpKey",
 *     "userNameAttribute",
 *     "userEmailAttribute",
 *     "defaultRoles",
 *     "autoProvision",
 *   },
 *   links = {
 *     "canonical" = "/admin/config/people/ssp-auth/identity_provider/{identity_provider}",
 *     "add-form" = "/admin/config/people/ssp-auth/identity_provider/add",
 *     "edit-form" = "/admin/config/people/ssp-auth/identity_provider/{identity_provider}/edit",
 *     "delete-form" = "/admin/config/people/ssp-auth/identity_provider/{identity_provider}/delete",
 *     "collection" = "/admin/config/people/ssp-auth/identity_provider"
 *   }
 * )
 */
class IdentityProvider extends ConfigEntityBase implements IdentityProviderInterface {

  /**
   * The Identity provider ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Identity provider label.
   *
   * @var string
   */
  protected $label;

  /**
   * The identity provider key as provided in the metadata entity id.
   *
   * @var string
   */
  protected $idpKey;

  /**
   * The user name attribute.
   *
   * @var string
   */
  protected $userNameAttribute;

  /**
   * The user mail attribute.
   *
   * @var string
   */
  protected $userEmailAttribute;

  /**
   * The default user roles.
   *
   * @var array
   */
  protected $defaultRoles;

  /**
   * If the user should be auto provisioned.
   *
   * @var bool
   */
  protected $autoProvision;

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function setId($id) {
    $this->id = $id;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel($label) {
    $this->label = $label;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getIdpKey() {
    return $this->idpKey;
  }

  /**
   * {@inheritdoc}
   */
  public function setIdpKey($idpKey) {
    $this->idpKey = $idpKey;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserNameAttribute() {
    return $this->userNameAttribute;
  }

  /**
   * {@inheritdoc}
   */
  public function setUserNameAttribute($userNameAttribute) {
    $this->userNameAttribute = $userNameAttribute;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserEmailAttribute() {
    return $this->userEmailAttribute;
  }

  /**
   * {@inheritdoc}
   */
  public function setUserEmailAttribute($userEmailAttribute) {
    $this->userEmailAttribute = $userEmailAttribute;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultRoles() {
    return $this->defaultRoles ? $this->defaultRoles : [];
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultRoles(array $defaultRoles) {
    $this->defaultRoles = $defaultRoles;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAutoProvision() {
    return $this->autoProvision;
  }

  /**
   * {@inheritdoc}
   */
  public function setAutoProvision($autoProvision) {
    $this->autoProvision = $autoProvision;

    return $this;
  }

}
