<?php

namespace Drupal\ssp_auth;

use Drupal\Core\Config\ConfigFactoryInterface;
use SimpleSAML\Auth\Simple;

/**
 * Class Authentication.
 */
class Authentication implements AuthenticationInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a Authentication service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Query if the current user is authenticated by SAML.
   *
   * @return bool
   *   TRUE if the user is current authenticated by SAML.
   */
  public function isAuthenticated() {
    return $this->authSimple !== NULL && $this->authSimple->isAuthenticated();
  }

  /**
   * Returns a SimpleSAML Simple class instance.
   *
   * @return \SimpleSAML\Auth\Simple|NULL
   *   The SimpleSAML Simple instance.
   */
  public function getSimpleSamlInstance() {
    if (!empty($this->instance)) {
      return $this->instance;
    }
    $config = $this->configFactory->get('ssp_auth.serviceprovidersettings');
    $auth_source = $config->get('service_provider_id');

    $this->instance = new Simple($auth_source);

    return $this->instance;
  }

}
